import path from 'path'

const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const deployPlugin = require('../scripts/deploy')

const config = {
  projectName: 'dp-mp-cli',
  date: '2022-8-22',
  designWidth: 750,
  deviceRatio: {
    640: 2.34 / 2,
    750: 1,
    828: 1.81 / 2,
  },
  sourceRoot: 'src',
  outputRoot: 'dist',
  alias: {
    '@': path.resolve(__dirname, '../src/'),
  },
  plugins: [],
  defineConstants: {
    /** 坑点 不用引号包一下 取不到这个值 本来想用import { client_id, appId } from './app' 有好的方法可以替换掉 */
    UDC_CLIENT_ID: '"354201"',
    WEAPP_APPID: '"wx4ac453415fc933cf"',
  },
  copy: {
    patterns: [],
    options: {},
  },
  framework: 'react',
  compiler: 'webpack5',
  cache: {
    enable: true, // Webpack 持久化缓存配置，建议开启。默认配置请参考：https://docs.taro.zone/docs/config-detail#cache
  },
  mini: {
    // 开启热更新
    hot: true,
    webpackChain(chain) {
      if (process.env.ENABLE_ANALYZER === 'true') {
        chain.plugin('analyzer').use(BundleAnalyzerPlugin, [])
      }
    },
    postcss: {
      pxtransform: {
        enable: true,
        config: {},
      },
      url: {
        enable: true,
        config: {
          limit: 1024, // 设定转换尺寸上限
        },
      },
      cssModules: {
        // 开启css模块化
        enable: true,
        config: {
          namingPattern: 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]',
        },
      },
    },
  },
}

/** 合并小程序的发布配置 */
config.plugins = [
  ['@tarojs/plugin-mini-ci', deployPlugin]
]

module.exports = function (merge) {
  if (process.env.NODE_ENV === 'development') {
    return merge({}, config, require('./dev'))
  }
  return merge({}, config, require('./prod'))
}
