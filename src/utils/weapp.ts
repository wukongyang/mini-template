/**
 * 小程序相关的api调用
 */
import Taro from '@tarojs/taro';
import { sleep } from './utils'
/**
 *
 * @param callback 小程序调用相机扫码
 */
export const scanCode = (callback: (res) => void) => {
  Taro.scanCode({
    success: (res) => {
      callback(res)
    },
    fail: (res) => {
      callback(res)
    }
  })
}

export interface IShowToast {
  title: string;
  duration?: number;
  icon?: 'success' | 'loading' | 'none' | 'error';
}

/**
   * 显示消息提示框 同步执行
   * @param title
   * @param icon
   * @param duration 默认2s
   */
export async function toastSync({
  title,
  icon = 'none',
  duration = 2000,
}: IShowToast): Promise<void> {
  if (!title) {
    return
  }
  await Taro.showToast({ title, icon, duration, mask: true });
  await sleep(duration);
  return;
}

/**
 * 小程序调用接口获取登录凭证(CODE)
 */
export async function wxLoginCode() {
  return new Promise<string>((resolve) => {
    Taro.login({
      success: function (res) {
        if (res.code) {
          resolve(res.code);
        } else {
          resolve('');
        }
      },
    });
  });
}
