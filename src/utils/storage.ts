import { IUser } from '@/service/types/user'
import Taro from '@tarojs/taro'

export const localStorageKey = 'com.drpanda.distributor.'

interface IStorage<T> {
  key: string
  defaultValue: T
}
export class Storage<T> implements IStorage<T> {
  key: string

  defaultValue: T

  constructor(key, defaultValue) {
    this.key = localStorageKey + key
    this.defaultValue = defaultValue
  }

  setItem(value: T) {
    Taro.setStorageSync(this.key, JSON.stringify(value))
  }

  getItem(): T {
    const value = Taro.getStorageSync(this.key)
    if (value === undefined) return this.defaultValue
    try {
      return value && value !== 'null' && value !== 'undefined'
        ? (JSON.parse(value) as T)
        : this.defaultValue
    } catch (error) {
      return value && value !== 'null' && value !== 'undefined'
        ? ((value as unknown) as T)
        : this.defaultValue
    }
  }

  removeItem() {
    Taro.removeStorageSync(this.key)
  }
}

/** 管理token */
export const tokenStorage = new Storage<string>('token', '')

/** 管理user */
export const userStorage = new Storage<IUser>('user', {})

/** 清除所有本地存储 */
export const clearStorage = () => {
  Taro.clearStorageSync()
}
