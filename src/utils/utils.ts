/**
 *
 * @param query 序列化对象为字符串拼接
 * for example {
 *   a: 1,
 *   b: 2
 * }
 * return a=1&b=2
 * @returns
 */
export function serializeObject(query: Record<string, unknown>): string {
  if (!query) {
    return ''
  }
  const newObj: Record<string, unknown> = (Object.keys(query) || [])
    .filter(key => query[key])
    .reduce((acc, key) => ({ ...acc, [key]: query[key] }), {})
  let str = ''
  for (const key in newObj) {
    str = `${str}${key}=${newObj[key]}&`
  }
  str = str.substring(0, str.length - 1)
  return str
}

export interface IChangeTime {
  d: number,
  h: number | string,
  m: number | string,
  s: number | string
}

/**
 * 时间戳转换时分秒
 * @param timestamp
 * @returns
 */
export function tranformTime2S(timestamp: number): IChangeTime {
  const date: IChangeTime = {
    d: 0,
    h: '',
    m: '',
    s: ''
  };

  date.d = Math.floor(timestamp / 1000 / 60 / 60 / 24);

  date.h = Math.floor((timestamp / 1000 / 60 / 60) % 24);
  date.h = date.h > 9 ? date.h : '0' + date.h;

  date.m = Math.floor((timestamp / 1000 / 60) % 60);
  date.m = date.m > 9 ? date.m : '0' + date.m;

  date.s = Math.floor((timestamp / 1000) % 60);
  date.s = date.s > 9 ? date.s : '0' + date.s;
  return date;
}


/**
  * 睡几秒~~~~
  * @param ms
  * @return {Promise<unknown>}
  */
export function sleep(ms: number): Promise<unknown> {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
