
/**
 * 提供udc集团登录的方案
 * 目前小程序获取手机号登录的流程必须走集团的sdk鉴权
 */
import { IUser } from '@/service/types/user';
import userApi from '@/service/user'
import runtime from './runtime'
import { toastSync } from './weapp'

const udcSDK = require('@udc/tal-passport-minisdk')


/**
 * 初始化SDK
 */
export const udcInit = async (): Promise<boolean> => {
  return new Promise(resolve => {
    udcSDK.config({
      client_id: UDC_CLIENT_ID,
      env: runtime.env === 'dev' ? 'sandbox' : 'production',
      mini_appid: WEAPP_APPID,
      loginout_callback: function () {
        toastSync({ title: '登录已失效，请重新登录' }).then(() => {
          // todo 全局登录失效处理
        })
      },
      mini_version: '1.0.0'
    })
    resolve(true)
  })
}

/**
 * 静默登录
 */
export const silentLogin = async (): Promise<IUser | null> => {
  return new Promise((resolve, reject) => {
    udcSDK.loginCodeByAuth({
      data: { bindType: 1 },
      success: (res) => {
        const { data: { errcode, data: { action, code }, errmsg } } = res
        if (errcode === 0) {
          if (action === 'logined') {
            userApi.udcLogin({ loginCode: code, clientId: UDC_CLIENT_ID }).then(res => resolve(res.data))
          } else {
            reject(null)
          }
        } else {
          reject(errmsg)
        }
      },
      fail: (err) => {
        console.warn('TUC loginCodeByAuth FAIL:', err)
        resolve(null)
      }
    })
  })
}

/**
 * 授权登录
 */
export async function authLogin({ encryptedData, iv }): Promise<boolean> {
  const params = { encryptedData, iv, bindType: 1 }
  return new Promise(resolve => {
    udcSDK.loginPhoneByAuth({
      data: params,
      success: (res) => {
        const { errcode, data } = res.data
        if (errcode === 0) {
          if (data.action === 'logined') {
            /** code登录 */
            userApi.udcLogin({ loginCode: data.code, clientId: UDC_CLIENT_ID }).then(res => {
              if (res.code === 0) {
                resolve(!!res.data?.id)
              } else {
                resolve(false)
              }
            })
          } else {
            resolve(false)
          }
        } else {
          toastSync({ title: `登录失败: ${errcode}` })
          resolve(false)
        }
      },
      fail: (err) => {
        toastSync({ title: err })
        resolve(false)
      },
    })
  })
}
