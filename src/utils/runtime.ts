export type RuntimeEnv = 'dev' | 'pre' | 'prod'

let env: RuntimeEnv = 'dev'
let isDevelopment: boolean = false
const { NODE_ENV } = process.env

if (NODE_ENV === 'development') {
  env = 'dev'
  isDevelopment = true
}

if (NODE_ENV === 'preview') {
  env = 'pre'
}

if (NODE_ENV === 'prod') {
  env = 'prod';
}

export default {
  isDevelopment,
  env,
}
