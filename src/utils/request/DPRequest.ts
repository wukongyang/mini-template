import Taro from '@tarojs/taro';
import { serializeObject } from '../utils'
import runtime from '../runtime'

/** 目前的接口类型只考虑GET和POST */
export type HttpMethod = 'GET' | 'POST'

/** 服务器环境域名区分 */
export interface IDomainEnv {
  dev: string
  pre: string
  prod: string
}

/** 定义panda服务器外的额外服务器字段 */
export interface IExtraResponse {
  // 外包的字段为msg
  msg?: string
  // 获取ip的接口只有ip
  ip?: string
}

export interface IHttpResponse<T = any> {
  ok: boolean
  data: T
  statusCode: number
  statusText: string
  header: any
  url?: string
}

export interface IResponse<T = any> extends IExtraResponse {
  code: number;
  data: T;
  message: string;
}

export interface ErrorEventType {
  code?: number
  url?: string
  errorText?: string
}

export interface IRequest<T = any> {
  /** request本身会根据path匹配域名 提供特殊情况走指定服务的情况 */
  baseUrl?: string
  /** 接口路径 */
  path?: string;
  /** 不需要鉴权的接口配置 可以在requestIntercepter处理一些特殊的header*/
  noAuth?: string;
  /** 请求方法 */
  method?: string;
  /** header config */
  header?: Record<string, string | number>
  /** post请求参数 */
  data?: T  // Object | string | File | Stream
  /** get请求参数 */
  query?: T
  /** 请求拦截 */
  requestIntercepter?: (config: IRequest) => IRequest
  /** 错误处理 */
  failHandler?: (error: ErrorEventType) => void
  /** 成功处理 */
  successHandler?: (success: IHttpResponse) => void
  /** 是否返回response的所有字段 默认是返回response.data */
  returnAllResponse?: boolean
}

export const DefaultHeaders = {
  credentials: 'include',
  mode: 'no-cors',
}

export enum PostType {
  JSON = 'application/json;charset=utf-8;',
  FROM = 'application/x-www-form-urlencoded;charset=utf-8;',
  FROM_DATA = 'multipart/formdata',
}

export type ServerType = 'OLA' | 'UIS' | 'FC' | 'DPPAD' | 'PAD'

export const ServerDomain: Record<ServerType, IDomainEnv> = {
  OLA: {
    dev: 'https://ola-dev.xiongmaoboshi.com',
    pre: 'https://ola-pre.xiongmaoboshi.com',
    prod: 'https://ola.xiongmaoboshi.com',
  },
  UIS: {
    dev: 'https://staging-subs.xiongmaoboshi.com',
    pre: 'https://subs-pre.xiongmaoboshi.com',
    prod: 'https://subs.xiongmaoboshi.com',
  },
  FC: {
    dev: 'https://fc.xiongmaoboshi.com/h5/dev',
    pre: 'https://fc.xiongmaoboshi.com/h5/pre',
    prod: 'https://fc.xiongmaoboshi.com/h5',
  },
  DPPAD: {
    dev: 'https://pad-api-dev.xiongmaoboshi.com/s/drpanda',
    pre: 'https://pad-api-pre.xiongmaoboshi.com/s/drpanda',
    prod: 'https://pad-api.xiongmaoboshi.com/s/drpanda',
  },
  PAD: {
    dev: 'http://gray.jxwxxkj.com',
    pre: 'http://gray.jxwxxkj.com',
    prod: 'https://api.jxwxxkj.com',
  },
};


class DPRequest {
  constructor(config: IRequest) {
    this.setConfig(config)
  }

  config: IRequest = {
    path: '',
    returnAllResponse: false
  }

  /** 设置接口请求实际config */
  setConfig(config: IRequest) {
    if (config.header) {
      /** 处理默认header和配置header合并 */
      config.header = {
        ...DefaultHeaders,
        ...config.header,
      }
    }
    this.config = {
      ...this.config,
      ...config,
    }
  }

  /**
   * 根据接口路径按照一定规定返回对应三个环境的服务
   * 目前有一些接口没有按照规则来定义域名对应的接口，如果发现的话记得跟服务器及时沟通，不然后续就需要花更多时间来维护🙃🙃🙃🙃🙃🙃
   * @param path
   * @returns
   */
  getDomainUrl(path: string): IDomainEnv {
    let domainUrl: IDomainEnv = {
      dev: '',
      pre: '',
      prod: '',

    }
    if (/\/(uis|ns|code)/.test(path)) {
      domainUrl = ServerDomain['UIS']
    }
    if (/\/api\//.test(path)) {
      domainUrl = ServerDomain['PAD']
    }
    if (/\/pad/.test(path)) {
      domainUrl = ServerDomain['DPPAD']
    }
    if (/\/tt-mp/.test(path)) {
      domainUrl = ServerDomain['FC']
    }
    if (/\/(shop-api|shop-admin)/.test(path)) {
      domainUrl = ServerDomain['OLA']
    }
    return domainUrl
  }

  /** 序列化GET请求参数 */
  getRealPath = (config: IRequest): string => {
    const { path = '', query, method } = config
    if (method !== 'GET') {
      return path
    }
    const serializeQuery = serializeObject(query)
    /** 处理path上写了参数 query也写了参数的情况 */
    if (path.includes('?') && serializeQuery) {
      return `${path}&${serializeQuery}`
    } else if (serializeQuery) {
      return `${path}?${serializeQuery}`
    }
    return path
  }

  formatBody = (data: Record<string, unknown>) => {
    let body: string = '';
    try {
      body = JSON.stringify(data || {});
    } catch (e) {
      console.warn('***formatBody error***', e)
    }
    return body;
  }

  /** 检测http请求状态 */
  checkStatus = (status: number) => status >= 200 && status < 300

  async request<T>(config: IRequest): Promise<IResponse<T>> {
    const { requestIntercepter } = this.config
    /**  处理某个接口单独改了配置的情况 */
    let realConfig: IRequest = {
      ...this.config,
      ...config,
      path: this.getRealPath(config),
      header: {
        ...this.config.header,
        ...config.header,
      }
    }
    /** 接口拦截 */
    if (requestIntercepter) {
      realConfig = requestIntercepter(realConfig)
    }
    let { baseUrl = '', path = '', successHandler, failHandler } = realConfig

    return new Promise(async (resolve) => {
      /** 定义执行失败的操作 */
      const fail = (res: IHttpResponse) => {
        failHandler && failHandler(res)
        // 服务器错误也会返回res.data
        resolve(res.data);
      }

      /** 定义执行成功的操作 */
      const success = (res: IHttpResponse) => {
        if (this.checkStatus(res.statusCode)) {
          /** 服务器正确的返回code=0 */
          if (res.data?.code) {
            failHandler && failHandler(res)
          } else {
            successHandler && successHandler(res)
          }
        } else {
          failHandler && failHandler(res.data)
        }
        // 默认处理是返回res.data
        resolve(res.data);
      };

      if (!baseUrl) {
        baseUrl = this.getDomainUrl(path)[runtime.env]
      }
      const options: Taro.request.Option<any> | any = {
        fail,
        success,
        ...realConfig,
        url: `${baseUrl}${path}`
      };
      /** 根据请求数据类型格式化body */
      if (options?.data) {
        options.data = this.formatBody(options.data);
      }
      Taro.request(options as Taro.request.Option<any>);
    });
  }

  get<T = any>(path: string, config?: IRequest) {
    return this.request<T>({
      path,
      method: 'GET',
      ...config,
    })
  }

  post<T = any>(path: string, data: any, config?: IRequest) {
    return this.request<T>({
      path,
      data,
      method: 'POST',
      ...config,
    })
  }
}

export default DPRequest
