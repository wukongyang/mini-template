/**
 *  初始化网络请求配置选项（如：url前缀、headers配置）
 *  网络拦截器（请求与响应）
 */
import { tokenStorage } from '../storage'
import { toastSync } from '../weapp'
import DPRequest, { IHttpResponse, IRequest, IResponse, PostType } from './DPRequest'

/** 处理接口失败 处理服务器code提供提示文本*/
const failHandler = (response: IResponse) => {
  let { data: { code, message = '' } } = response
  switch (code) {
    case 404: {
      message = '服务器404错误'
    }
    default: {
      message = `${code}`
    }
  }
  toastSync({ title: message })
}

/** 处理接口成功 */
const successHandler = (response: IHttpResponse) => {
  const token = response?.header?.Authorization || response?.header?.authorization
  if (token) {
    tokenStorage.setItem(token)
  }
}

/** 发起请求前对request config做一些单独的处理 */
const requestIntercepter = (config: IRequest) => {
  const { path = '' } = config
  let header: Record<string, string | number> = {
    Authorization: tokenStorage.getItem(),
    Accept: PostType.JSON,
    'x-miniapp-appid': WEAPP_APPID,
    'Content-Type': PostType.JSON
  }
  // 针对某些接口做一些单独的处理
  if (path === '/uis/ns/account/udc-login-register-bind') {
    header = {
      ...header,
      'X-USER-API-VERSION': 3
    }
  }
  return {
    ...config,
    header,
  }
}

export default new DPRequest({
  requestIntercepter,
  successHandler,
  failHandler,
})
