import React, { useEffect } from 'react'
import { StoreProvider, useModel } from '@/store'
import userApi from '@/service/user'
import { silentLogin, udcInit } from './utils/udc'

interface IAppProps {
  children: React.ReactNode
}

const CommonEntrance = () => {
  const { setUserInfo } = useModel('user')
  useEffect(() => {
    // 全局初始化udc sdk  走静默登录
    udcInit().then(status => {
      if (status) {
        silentLogin()
          .then(data => {
            console.warn('silentLogin', data)
            setUserInfo(data)
          })
          .catch(err => {
            console.warn('err', err)
          })
      }
    })
    userApi.weAppLogin().then(res => {
      console.warn('weAppLogin', res)
    })
  }, [])
  return null
}

const App: React.FC<IAppProps> = props => {
  return (
    <StoreProvider>
      {props.children}
      <CommonEntrance />
    </StoreProvider>
  )
}

export default App
