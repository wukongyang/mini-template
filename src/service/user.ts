import { wxLoginCode } from '@/utils/weapp';
import request from '../utils/request';
import { IWxLogin, IWxDecryptParams, IWxDecrypt, IUser } from './types/user'

export default {
  /** 小程序登录 获取openid等 */
  weAppLogin: async () => {
    const code = await wxLoginCode();
    return request.get<IWxLogin>(`/h5/tool/wechat/openId?appId=${WEAPP_APPID}&code=${code}`, {
      baseUrl: 'https://fc.xiongmaoboshi.com',
    });
  },
  /** 微信解密 */
  wxDecrypt: (params: IWxDecryptParams) => request.post<IWxDecrypt>('/h5/tool/wechat/decrypt', params, {
    baseUrl: 'https://fc.xiongmaoboshi.com'
  }),
  /** udcLogin */
  udcLogin: ({ loginCode, clientId }) => request.post<IUser>('/uis/ns/account/udc-login-register-bind', {
    loginCode,
    clientId
  })
}
