export interface IUser {
  id: string;
  avatar: string;
  unionId: string;
  openId: string;
  name: string;
  mobile: string;
  gzhSubscribed: boolean;
  newAccount: boolean;
}

export interface IWxLogin {
  session_key: string;
  openid: string;
  unionid: string;
}

export interface IWxDecryptParams {
  sessionKey: string;
  appId: string;
  encryptedData: string;
  iv: string;
}

export interface IWxDecrypt {
  phoneNumber: string;
  purePhoneNumber: string;
  countryCode: string;
  watermark: {
    timestamp: number;
    appid: string;
  };
}
