import { IUser } from '@/service/types/user'
import { useState } from 'react'

export default () => {
  const [userInfo, setUserInfo] = useState<Partial<IUser> | null>(null)
  const [token, setToken] = useState<string>('')


  return { userInfo, setUserInfo }
}
