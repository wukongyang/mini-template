import { useEffect, useRef } from 'react'

import { useMemoizedFn } from '.'

interface ICurrentRef<T> {
  fn: T
  timer: NodeJS.Timer | null
}

/** 节流 */
function useThrottle<T extends Function>(fn: T, delay: number) {
  const { current } = useRef<ICurrentRef<T>>({ fn, timer: null })
  useEffect(() => {
    current.fn = fn
  }, [fn])

  return useMemoizedFn(function f(...args) {
    if (!current.timer) {
      current.timer = setTimeout(() => {
        current.timer = null
      }, delay)
      current.fn.call(this, ...args)
    }
  })
}

export default useThrottle
