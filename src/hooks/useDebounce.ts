import { useEffect, useRef } from 'react'

import { useMemoizedFn } from '.'

interface ICurrentRef<T> {
  fn: T
  timer: NodeJS.Timer | null
}

/** 防抖 */
function useDebounce<T extends Function>(fn: T, delay: number): T {
  const { current } = useRef<ICurrentRef<T>>({ fn, timer: null })

  useEffect(() => {
    current.fn = fn
  }, [fn])

  return useMemoizedFn((...args) => {
    if (current.timer) {
      clearTimeout(current.timer)
    }
    current.timer = setTimeout(() => {
      current.fn.call(null, ...args)
    }, delay)
  }) as unknown as T
}

export default useDebounce
