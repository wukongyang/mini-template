import { Config } from '@tarojs/taro'

export default {
  pages: ['pages/index/index'],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: '熊猫博士小程序',
    navigationBarTextStyle: 'black',
  },
} as Config
