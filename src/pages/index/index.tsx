import { View, Text } from '@tarojs/components'
import './index.module.less'

const Index = () => {
  return (
    <View className='index'>
      <Text>test</Text>
    </View>
  )
}

export default Index
