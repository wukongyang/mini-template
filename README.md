# dp-mp-cli

熊猫博士小程序项目脚手架

### 规范和使用文档输出文档站点，方便查阅。

todo 待更新到语雀文档

### 开发

node 版本：可使用 16.13.2

- 安装依赖

  ```bash
  npm i
  ```

- dev

  ```bash
  npm run dev:weapp
  ```

- build

```bash
  npm run build:weapp
```

### share

- 升级最新开发者工具后，在 vscode 中编写代码不热更新，请问怎么破？ [https://developers.weixin.qq.com/community/develop/doc/000a4036bf0660be5f3a4ca3b56400?highLine=%25E5%258D%2587%25E7%25BA%25A7%25E6%259C%2580%25E6%2596%25B0%25E5%25BC%2580%25E5%258F%2591%25E8%2580%2585%25E5%25B7%25A5%25E5%2585%25B7%25E5%2590%258E%25EF%25BC%258C%25E5%259C%25A8vscode%2520%25E4%25B8%25AD%25E7%25BC%2596%25E5%2586%2599%25E4%25BB%25A3%25E7%25A0%2581%25E4%25B8%258D%25E7%2583%25AD%25E6%259B%25B4%25E6%2596%25B0%25EF%25BC%258C%25E8%25AF%25B7%25E9%2597%25AE%25E6%2580%258E%25E4%25B9%2588%25E7%25A0%25B4%25EF%25BC%259F]
