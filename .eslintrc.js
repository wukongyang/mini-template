module.exports = {
  "extends": [
    "taro/react",
    "prettier"
  ],
  "rules": {
    "react/jsx-uses-react": "off",
    "react/react-in-jsx-scope": "off",
    "import/no-commonjs": "off",
    "react-hooks/exhaustive-deps": "off",
    "@typescript-eslint/no-shadow": "off"
  }
}
