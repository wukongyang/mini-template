const { APP_INFO: { appId, version, description } } = require('../config/app')

/** 定义小程序项目发布部署的配置 */
module.exports = {
  weapp: {
    appid: appId,
    projectPath: 'dist',
    /** 定义发布上传的秘钥 */
    // todo 更新/key目录下秘钥的文件名为appId
    privateKeyPath: `scripts/key/private.${appId}.key`,
  },
  version,
  desc: description,
}
